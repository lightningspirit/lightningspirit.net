<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lightningspiritnet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'silva1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XcD_/RV>n,b>:Zp=INx3d2CObJ;F] ZxiQ+|L{7. Pf-oJ6+-`(;X9+T4-C*P*N@');
define('SECURE_AUTH_KEY',  '@,p(-?c48!>KH35U*pDCd_=1NQ m}EYo/NFBMf{I^xIqso*ds1dp{q|1eAG9|!0i');
define('LOGGED_IN_KEY',    'm]u+r&Xx%a)EKeuHh+lj_,^3B=W+5iLwgo-Ka0ijhk0/{L>i}iw=H~DINC{A&}^z');
define('NONCE_KEY',        ';P<t<#=bO)eP&V5-M>R~CK~(o1C,EBsJ4tN|]v%dM6U]%Xj?HhzJg&IWKfiKl]>0');
define('AUTH_SALT',        '4wd~i+5G.g/=NAa-qu$nH;o`8[g|K1an8kM&LMA</%<$eSvMYQNjjE+*,@^B2?_!');
define('SECURE_AUTH_SALT', 'Ra_]`Te@TQ;1Kvc2vlM|C[PALwXRVUY6FJ9taeTB!_CO*OlDVGH@M`ab#CxA^A z');
define('LOGGED_IN_SALT',   '3[Yt>x$_*sr7Dpgd JsE(z;8ApOg=:bWX|Ey`TN-)*G8L.}Mr:r* #~^Hb>^x*Qg');
define('NONCE_SALT',       'VivQiMZE|M4jN`+@jJ>3;Kc8v/lH]?}i (tf3#]8T=?lFo,$0&&p7BVAwu~}EPl+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lhgst_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
